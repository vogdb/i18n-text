I18nText.error = {
  NO_OPTS: 'Options are not present',
  NO_PATH: 'Options path is not present',
  NO_MESSAGES: '{{file}} is unreachable. Error {{error}}',
  EMPTY_MESSAGES: '{{file}} is empty',
  INVALID_KEY: '{{key}} key is not present in locale {{locale}}',
  NO_LOCALE_IS_SET: 'Locale is not set.',
  INVALID_PLURAL: 'Invalid plural form: {{plural}}'
}

I18nText.event = {
  LOCALE_LOAD: 'localeload',
  LOCALE_CHANGE: 'localechange'
}

I18nText.KEY_SEPARATOR = '.'