function isObject(arg) {
  return Object.prototype.toString.call(arg) === '[object Object]'
}

function replaceAll(text, params) {
  return text.replace(/{{([\w]+)}}/g, function (match, paramName) {
    var hit = params[paramName]
    if (hit) {
      return hit
    } else {
      return match
    }
  })
}

function error(msg, params) {
  throw new Error(replaceAll(msg, params))
}