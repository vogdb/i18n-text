var Plural = {
  parseAndEval: function (text, n) {
    return text.replace(/{{([^{}]+#[^}]*(?:|.*#.*)+)}}/g, function (match, pluralConcat) {
      return Plural.eval(pluralConcat, n)
    })
  },
  eval: function (pluralConcat, n) {
    var plurals = pluralConcat.split('|')
    for (var i = plurals.length - 1; i >= 0; i--) {
      var pluralParts = plurals[i].split('#')
      var formula = pluralParts[0]
      if (!Plural.isValidFormula(formula)) {
        error(I18nText.error.INVALID_PLURAL, {plural: formula})
      }
      if (Plural.evalFormula(formula, n)) {
        return pluralParts[1]
      }
    }
    return pluralConcat
  },
  isValidFormula: function (formula) {
    //TODO test it fairly
    return /[ \dn<>=*-+!?]+/g.test(formula)
  },
  evalFormula: function (formula, n) {
    return (new Function('n', 'return ' + formula)(n))
  }
}