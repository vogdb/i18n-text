var LOCALES = {
  EN: 'en',
  RU: 'ru',
  DE: 'de'
}

var MESSAGES_PATH = 'messages'

function i18nKey() {
  return Array.prototype.join.call(arguments, I18nText.KEY_SEPARATOR)
}

function stripParams(string) {
  return string.replace(/{{[\w]+}}/g, '')
}

/**
 * Phantom shim
 */
if (!Function.prototype.bind) {
  Function.prototype.bind = function (that) {
    var fn = this
    var bindedArgs = Array.prototype.slice.call(arguments, 1)
    return function () {
      var args = Array.prototype.slice.call(arguments, 0)
      fn.apply(that, bindedArgs.concat(args))
    }
  }
}