/*!
 i18n-text - v0.4.3 - 2014-08-14
 https://vogdb@bitbucket.org/vogdb/i18n-text.git
 Copyright (c) 2014 Sanin Aleksey aka vogdb; Licensed WTFPL
*/
;(function( global ) {
function isObject(arg) {
  return Object.prototype.toString.call(arg) === '[object Object]'
}

function replaceAll(text, params) {
  return text.replace(/{{([\w]+)}}/g, function (match, paramName) {
    var hit = params[paramName]
    if (hit) {
      return hit
    } else {
      return match
    }
  })
}

function error(msg, params) {
  throw new Error(replaceAll(msg, params))
}
var Plural = {
  parseAndEval: function (text, n) {
    return text.replace(/{{([^{}]+#[^}]*(?:|.*#.*)+)}}/g, function (match, pluralConcat) {
      return Plural.eval(pluralConcat, n)
    })
  },
  eval: function (pluralConcat, n) {
    var plurals = pluralConcat.split('|')
    for (var i = plurals.length - 1; i >= 0; i--) {
      var pluralParts = plurals[i].split('#')
      var formula = pluralParts[0]
      if (!Plural.isValidFormula(formula)) {
        error(I18nText.error.INVALID_PLURAL, {plural: formula})
      }
      if (Plural.evalFormula(formula, n)) {
        return pluralParts[1]
      }
    }
    return pluralConcat
  },
  isValidFormula: function (formula) {
    //TODO test it fairly
    return /[ \dn<>=*-+!?]+/g.test(formula)
  },
  evalFormula: function (formula, n) {
    return (new Function('n', 'return ' + formula)(n))
  }
}
/**
 * @param opts options.
 * @param {String} opts.path path where messages files are located.
 * @param {String} [opts.locale] the locale id which will be passed to setLocale()
 * at the end of construction.
 * @constructor
 */
var I18nText = function (opts) {
  this._loadedLocales = {}
  this._currentLocale = null
  this._msgPath = null
  this._subscribers = {}

  this._init(opts)
}

/**
 * @return {String} the current locale id.
 */
I18nText.prototype.getLocale = function () {
  return this._currentLocale
}

/**
 * Set the current locale. The locale sets immediately only if it is already loaded.
 * Use <code>i18n.on(I18nText.event.LOCALE_LOAD, function (data) {})</code> to check when locale is set.
 * @param {String} locale id.
 */
I18nText.prototype.setLocale = function (locale) {
  if (this.hasLocale(locale)) {
    this._setLocale(locale)
  } else {
    this.once(I18nText.event.LOCALE_LOAD, function (data) {
      if (!data.error && data.locale === locale) {
        this._setLocale(data.locale)
      }
    }.bind(this))
    this.loadLocale(locale)
  }
}

I18nText.prototype._setLocale = function (locale) {
  this._currentLocale = locale
  this._fire(I18nText.event.LOCALE_CHANGE, {locale: locale})
}

/**
 * @param {String} locale the locale id.
 * @return {Boolean} whether the messages file for the locale has been loaded.
 */
I18nText.prototype.hasLocale = function (locale) {
  return !!this._loadedLocales[locale]
}

/**
 * Loads the messages file for the locale. The messages file should be in JSON
 * format and has the same name as the locale id. For example, the 'en' locale
 * id must have the 'en.json' file.
 * @param {String} locale locale id.
 * @param {String} [path] path where the messages file for the locale is located.
 */
I18nText.prototype.loadLocale = function (locale, path) {
  path = path || this._msgPath
  var file = path + '/' + locale + '.json'
  I18nText.loadFile({
    url: file,
    success: function (content) {
      if (content.length > 0) {
        this._loadedLocales[locale] = {}
        this._mergeKeys(JSON.parse(content), this._loadedLocales[locale], '')
        this._fire(I18nText.event.LOCALE_LOAD, {locale: locale})
      } else {
        this._fire(
          I18nText.event.LOCALE_LOAD,
          {error: replaceAll(I18nText.error.EMPTY_MESSAGES, {file: file})}
        )
      }
    }.bind(this),
    error: function (msg) {
      this._fire(
        I18nText.event.LOCALE_LOAD,
        {error: replaceAll(I18nText.error.NO_MESSAGES, {file: file, error: msg})}
      )
    }.bind(this)
  })
}

I18nText.prototype._mergeKeys = function (messages, storage, prefix) {
  for (var key in messages) {
    if (isObject(messages[key])) {
      var upperPrefix
      if (prefix) {
        upperPrefix = prefix + I18nText.KEY_SEPARATOR + key
      } else {
        upperPrefix = key
      }
      this._mergeKeys(messages[key], storage, upperPrefix)
    } else {
      var mergedKey
      if (prefix) {
        mergedKey = prefix + I18nText.KEY_SEPARATOR + key
      } else {
        mergedKey = key
      }
      storage[mergedKey] = messages[key]
    }
  }
}

/**
 * @param {String} key the key in the messages file.
 * @param {Object.<string, Object>} [params] values to be used in translation.
 * @param {Number} [params.n] Reserved param for the plural calculation. See 'whiteBox.js' test for example.
 * @param {String} [customLocale] the locale id. If param is omitted locale == getLocale().
 * @return {String} locale text that corresponds to the key.
 */
I18nText.prototype.text = function (key, params, customLocale) {
  if (arguments[1] && !isObject(arguments[1])) {
    customLocale = arguments[1]
    params = undefined
  }
  var locale = customLocale || this.getLocale()
  if (!locale) {
    error(I18nText.error.NO_LOCALE_IS_SET)
  }
  if (!this.hasLocale(locale)) {
    this.loadLocale(locale)
  }
  var result = this._loadedLocales[locale][key]
  if (result === undefined) {
    error(I18nText.error.INVALID_KEY, {key: key, locale: locale})
  }
  if (params) {
    if (params['n'] !== undefined) {
      result = Plural.parseAndEval(result, params['n'])
    }
    return replaceAll(result, params)
  }
  return result
}

/**
 * Attach an event handler function for event.
 * @param {I18nText.event} event.
 * @param {Function} callback function to execute when the event is triggered.
 * It has only one argument {Object} data. Argument data is:
 * {locale: locale that was loaded/changed, error: error message if error occurs}
 */
I18nText.prototype.on = function (event, callback) {
  if (!this._subscribers[event]) {
    this._subscribers[event] = []
  }
  this._subscribers[event].push(callback)
}

I18nText.prototype._fire = function (event, data) {
  if (!this._subscribers[event]) {
    return
  }
  var subscribers = this._subscribers[event]
  for (var i = subscribers.length - 1; i >= 0; i--) {
    subscribers[i].call(null, data)
  }
}

/**
 * Remove an event handler.
 * @param {I18nText.event} event
 * @param {Function} [callback] handler function previously attached for the event.
 * If it is omitted then all callbacks for event are removed.
 */
I18nText.prototype.off = function (event, callback) {
  if (!this._subscribers[event]) {
    return
  }
  if (callback) {
    var callbackIndex = this._subscribers[event].indexOf(callback)
    this._subscribers[event].splice(callbackIndex, 1)
  }
  if (!callback || this._subscribers.length === 0) {
    delete this._subscribers[event]
  }
}

/**
 * Attach an event handler function for event. The handler is executed only once.
 * See the previous 'on' method.
 */
I18nText.prototype.once = function (event, callback) {
  this.on(event, function (data) {
    this.off(event, callback)
    callback(data)
  }.bind(this))
}

I18nText.prototype._init = function (opts) {
  if (!opts) {
    error(I18nText.error.NO_OPTS)
  }
  if (opts['path']) {
    this._msgPath = opts['path']
  } else {
    error(I18nText.error.NO_PATH)
  }
}
function containLocalFileUrl(string) {
  return string.indexOf('file://') !== -1
}

function isLocalFileRequest(url) {
  return containLocalFileUrl(url) || (containLocalFileUrl(window.location.href) && url.indexOf('http://' === -1))
}

/**
 * Utility function that loads file.
 * @param {Object} options
 * @param {string} options.url file to load
 * @param {Function} options.success file is loaded
 * @param {Function} options.error file can't be loaded
 */
I18nText.loadFile = function (options) {
  var request = new XMLHttpRequest()
  request.open('GET', options.url)
  request.onreadystatechange = function () {
    if (request.readyState === 4) {
      if (request.status == 200 || (isLocalFileRequest(options.url) && request.responseText.length > 0)) {
        options.success(request.responseText)
      } else {
        options.error(request.responseText)
      }
    }
  }
  request.send(null)
}
I18nText.error = {
  NO_OPTS: 'Options are not present',
  NO_PATH: 'Options path is not present',
  NO_MESSAGES: '{{file}} is unreachable. Error {{error}}',
  EMPTY_MESSAGES: '{{file}} is empty',
  INVALID_KEY: '{{key}} key is not present in locale {{locale}}',
  NO_LOCALE_IS_SET: 'Locale is not set.',
  INVALID_PLURAL: 'Invalid plural form: {{plural}}'
}

I18nText.event = {
  LOCALE_LOAD: 'localeload',
  LOCALE_CHANGE: 'localechange'
}

I18nText.KEY_SEPARATOR = '.'
  if ( typeof define === "function" && define.amd ) {
    define( function () { return I18nText } )
  } else {
    global['I18nText'] = I18nText
  }
}(typeof window === 'undefined' ? this : window))