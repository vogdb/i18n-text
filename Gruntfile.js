module.exports = function (grunt) {

  grunt.initConfig({

    pkg: grunt.file.readJSON('package.json')

    ,banner: '/*!\n <%= pkg.title || pkg.name %> - v<%= pkg.version %> - '
      + '<%= grunt.template.today("yyyy-mm-dd") %>\n'
      + ' <%= (pkg.homepage ? "* " + pkg.homepage : pkg.repository.url) + "\\n" %>'
      + ' Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author %>;'
      + ' Licensed <%= _.pluck(pkg.licenses, "type").join(", ") %>\n*/\n'

    ,concat: {
      options:{
        banner: '<%= banner %>'
      }
      ,browser: {
        src: ['src/wrappers/prefix.js', 'src/helpers.js', 'src/Plural.js', 'src/I18nText.js', 'src/loadFile/xhr.js', 'src/const.js', 'src/wrappers/suffix-browser.js']
        ,dest: 'dist/<%= pkg.name %>.js'
      }
      ,node: {
        src: ['src/wrappers/prefix.js', 'src/helpers.js', 'src/Plural.js', 'src/I18nText.js', 'src/loadFile/node.js', 'src/const.js', 'src/wrappers/suffix-node.js']
        ,dest: 'lib/<%= pkg.name %>.js'
      }
    }

    ,watch: {
      src: {
        files: '<%= jshint.src.src %>'
        ,tasks: 'default'
      }
      ,gruntfile: {
        files: '<%= jshint.gruntfile.src %>'
        ,tasks: ['jshint:gruntfile']
      }
    }

    ,mocha: {
      options: {
        bail: true
        ,log: true
        ,mocha: {
          ignoreLeaks: false
        }
        ,run: true
      }
      ,no_min: {
        src: 'test/no_min.html'
      }
      ,min: {
        src: 'test/min.html'
      }
    }

/* Disabled because it will make I18nText.prototype unable to be properly extended.
    To include in package.json
    "superstartup-closure-compiler": "~0.1.4",
    "grunt-closure-tools": "git://github.com/vogdb/grunt-closure-tools#useGoogExport",

    ,closureCompiler: {
      options: {
        compilerFile: 'node_modules/superstartup-closure-compiler/build/compiler.jar'
        ,compilerOpts: {
          compilation_level: 'ADVANCED_OPTIMIZATIONS'
          ,useGoogExport: true
          ,output_wrapper: '"<%= banner %>;(function(window){%output%}(window))"'
        }
      }
      ,min: {
        src: ['<%= concat.dist.src %>', 'src/exports.js']
        ,dest: 'dist/<%= pkg.name %>.min.js'
      }
    }
*/

    ,uglify: {
      options:{
        banner: '<%= banner %>'
      }
      ,browser: {
        files: {
          'dist/<%= pkg.name %>.min.js': 'dist/<%= pkg.name %>.js'
        }
      }
    }

    ,jsdoc: {
      dist: {
        src: ['src/I18nText.js'],
        options: {
          destination: 'doc'
        }
      }
    }

  })

  grunt.loadNpmTasks('grunt-contrib-concat')
  grunt.loadNpmTasks('grunt-contrib-watch')
  grunt.loadNpmTasks('grunt-mocha')
  grunt.loadNpmTasks('grunt-mocha-test')
  //grunt.loadNpmTasks('grunt-closure-tools')
  grunt.loadNpmTasks('grunt-contrib-uglify')
  grunt.loadNpmTasks('grunt-jsdoc')

  grunt.registerTask('default', ['concat', 'mocha:no_min', 'uglify', 'mocha:min'])

}
